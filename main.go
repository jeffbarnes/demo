package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Update June 2020</h1>")
}

func main() {
	http.HandleFunc("/", handler)

	fmt.Println("Started, serving at 8500")
	err := http.ListenAndServe(":8500", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
